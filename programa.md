## Programa 2023-2024

### Clases presenciales

* Martes 9:00 a 11:00
* Jueves 11:00 a 13:00

### Sesiones

25 sesiones en total:

| Martes | Tema                 | Jueves  | Tema          |
|--------|----------------------|---------|---------------|
| Sep 12 | Introducción         | Sep 14  | NO LECTIVO    |
| Sep 19 | NO LECTIVO           | Sep 21  | Entorno I      |
| Sep 26 | Estructuras control  | Sep 28  | NO LECTIVO     |
| Oct 3  | Entorno I            | Oct 5   | Entorno II     |
| Oct 10 | Estructuras control  | Oct 12  | NO LECTIVO    |
| Oct 17 | Entorno II           | Oct 19  | Entorno III    |
| Oct 24 | Divide y vencerás    | Oct 26  | Algoritmos I   |
| Oct 31 | Divide y vencerás    | Nov 2   | Algoritmos I   |
| Nov 7  | Estructuras datos I  | Nov 9   | Algoritmos I   |
| Nov 14 | Estructuras datos I  | Nov 16  | Algoritmos II  |
| Nov 21 | Estructuras datos II | Nov 23  | Algoritmos II  |
| Nov 28 | Estructuras datos II | Nov 30  | Proyecto final |
| Dic 5  | Gestión de Ficheros  | Dic 7   | NO LECTIVO     |
| Dic 12 | Orientación objetos  | Dic 14  | Algoritmos III |
| Dic 19 | Eficiencia           | Dic 21  | Proyecto final |

### Frikiminutos

[Temario de Frikiminutos](https://gitlab.eif.urjc.es/cursoprogram/materiales/-/blob/main/frikiminutos/README.md)

### Programa

* Introducción a la programación y la informática (temas cubiertos en transparencias):
  * Presentación de la asignatura
  * Motivación
  * Instrucciones
  * Variables
  * Expresiones
* [El entorno de programación I](https://gitlab.eif.urjc.es/cursoprogram/materiales/-/blob/main/practicas/entorno-i/README.md):
  * El entorno Linux del laboratorio
  * Introducción a la shell (intérprete de comandos)
  * El IDE (Integrated Development Environment): PyCharm
* [Estructuras de control](https://gitlab.eif.urjc.es/cursoprogram/materiales/-/blob/main/practicas/control/README.md):
  * secuencia
  * condiciones
  * bucles
  * excepciones
* [El entorno de programación II](https://gitlab.eif.urjc.es/cursoprogram/materiales/-/blob/main/practicas/entorno-ii/README.md):
  * Depuración (debugging)
  * Pruebas (testing) y aserciones (assertions)
* Divide y vencerás:
  * funciones
  * ámbitos de las variables
* [El entorno de programación III](https://gitlab.eif.urjc.es/cursoprogram/materiales/-/blob/main/practicas/entorno-iii/README.md):
  * Sistemas de control de versiones: Git
  * Servicios de apoyo al desarrollo: GitLab
  * Instalación de paquetes Python: pip
* Estructuras de datos I:
  * Listas
  * Tuplas
* [Algoritmos I](https://gitlab.eif.urjc.es/cursoprogram/materiales/-/blob/main/practicas/algoritmos-i/README.md):
  * Ordenación
  * Manipulación de cadenas
  * Aproximaciones
  * Búsqueda binaria
* Estructuras de datos II:
  * diccionarios
  * combinación de estructuras
  * punteros y gestión de memoria
* [Algoritmos II](https://gitlab.eif.urjc.es/cursoprogram/materiales/-/blob/main/practicas/algoritmos-ii/README.md):
  * Ordenación
  * Aproximación
* Gestión de ficheros
* Programación orientada a objectos
  * Clases y objetos (instanciación de clases)
  * Herencia
* [Algoritmos III](https://gitlab.eif.urjc.es/cursoprogram/materiales/-/blob/main/practicas/algoritmos-iii/README.md):
  * Búsqueda
* Eficiencia:
  * Complejidad
  * Tipos de complejidad
  * Complejidad para algunos algoritmos
* [Proyecto final](https://gitlab.eif.urjc.es/cursoprogram/materiales/-/blob/main/practicas/final/README.md)
